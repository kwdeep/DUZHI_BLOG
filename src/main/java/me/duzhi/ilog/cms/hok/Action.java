package me.duzhi.ilog.cms.hok;

/**
 * 基础处理类
 * @author ashang.peng@aliyun.com
 * @date 二月 14, 2017
 */

public abstract class Action {
    /**
     * The next handler
     */
    protected Action next;


    public abstract <T> void handle(HokInvoke.Message message, HokInvoke.Inv<T> invoke, String action);

    public <T extends Action> T addAction(Class<T> actionClass ) throws IllegalAccessException, InstantiationException {
        this.next = actionClass.newInstance();
        return (T) next;
    }
}
